/**
 * Created by Jecky.Kansara on 14-02-2017.
 */
package com.automationanywhere.testapplication;
import com.sun.deploy.panel.JavaPanel;
import com.sun.image.codec.jpeg.JPEGEncodeParam;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
public class TestApplication {

    private JFrame frame;

    public TestApplication() {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                frame = new JFrame("Java Test Application");
                frame.setSize(550,550);
                frame.setVisible(true);
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setLayout(new BorderLayout());

                /*TextBoxPanel panel = new TextBoxPanel();
                panel.setPreferredSize(new Dimension(100,50));
                panel.setVisible(true);*/

                /*JPanel panel = new JPanel();
                panel.setBorder(BorderFactory.createEtchedBorder());
                panel.setPreferredSize(new Dimension(10,10));
                panel.add(new TextBoxPanel());*/

                JPanel toolBarPanel = new JPanel();
                toolBarPanel.setPreferredSize(new Dimension(50,50));
                toolBarPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
                toolBarPanel.setVisible(true);

                ToolBarMouseAdapter toolBarListener = new ToolBarMouseAdapter(frame);
                JToggleButton btn = new JToggleButton("TextBox");
                btn.setPreferredSize(new Dimension(60,45));
                btn.setName("ToolBar_TextBox");
                btn.addMouseListener(toolBarListener);
                btn.setBorder(BorderFactory.createEtchedBorder());

                JToggleButton tabToggle = new JToggleButton("Tab");
                tabToggle.setPreferredSize(new Dimension(60,45));
                tabToggle.setName("ToolBar_Tab");
                tabToggle.setBorder(BorderFactory.createEtchedBorder());
                tabToggle.addMouseListener(toolBarListener);

                toolBarPanel.add(btn);
                toolBarPanel.add(tabToggle);

                frame.add(BorderLayout.NORTH, toolBarPanel);
                frame.add(BorderLayout.CENTER, new TextBoxPanel());
                //frame.add(BorderLayout.CENTER, samplePanel);
            }
        });

    }

    public static void main(String[] args)
    {
        TestApplication testApplication = new TestApplication();
    }
}
