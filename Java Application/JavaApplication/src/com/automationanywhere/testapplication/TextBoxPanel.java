package com.automationanywhere.testapplication; /**
 * Created by Jecky.Kansara on 14-02-2017.
 */
import com.sun.deploy.panel.JavaPanel;
import com.sun.org.apache.xml.internal.resolver.helpers.BootstrapResolver;
import com.sun.xml.internal.bind.v2.runtime.reflect.opt.FieldAccessor_Long;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.MouseInputAdapter;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.geom.Arc2D;
import java.awt.geom.CubicCurve2D;

public class TextBoxPanel extends JPanel {
    JLabel _label;

    public TextBoxPanel() {
        super();

        this.setBorder(BorderFactory.createEtchedBorder());
        this.setLocation(0,0);
        this.setPreferredSize(new Dimension(10,10));
        this.setLayout(new BorderLayout());

        _label = new JLabel("Output of textbox.");
        _label.setVisible(true);
        _label.setLocation(200,200);


        JPanel panel = new JPanel();
        panel.setVisible(true);
        panel.setLayout(null);
        panel.setPreferredSize(new Dimension(10,10));
        panel.setBorder(BorderFactory.createEtchedBorder());


        panel.add(createSimpleTextBox());
        panel.add(createReadOnlyTextBox());
        panel.add(createDisabledTextBox());
        panel.add(createPasswordTextBox());
        panel.add(createTextArea());
        panel.add(createResizableTextBox());

        this.add(BorderLayout.CENTER, panel);
        this.add(BorderLayout.SOUTH, _label);
    }

    public Dimension getPreferredSize(){
        return new Dimension(300,300);
    }

    private JPanel createSimpleTextBox() {
        JTextField _simpleTextField = new JTextField();
        _simpleTextField.setPreferredSize(new Dimension(190,25));

        _simpleTextField.setVisible(true);
        _simpleTextField.setBackground(Color.white);
        _simpleTextField.setText("Simple Text Box");
        _simpleTextField.setName("SimpleTextBox");
        _simpleTextField.addKeyListener(new TextBoxKeyListener(_label, _simpleTextField));
        _simpleTextField.addMouseListener(new TextBoxMouseAdapter(_label, _simpleTextField));

        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout(FlowLayout.LEFT));
        panel.setLocation(10,10);
        panel.setSize(300,30);
        panel.add(new Label("Simple TextBox"));
        panel.add(_simpleTextField);

        return panel;
    }

    private JPanel createPasswordTextBox() {
        final JPasswordField _passwordTextField = new JPasswordField(20);
        _passwordTextField.setVisible(true);

        _passwordTextField.setPreferredSize(new Dimension(190,25));
        //_passwordTextField.setLocation(10,100);
        //_passwordTextField.setSize(300,25);

        _passwordTextField.setBackground(Color.white);
        _passwordTextField.setText("Password Text Box");
        _passwordTextField.setName("PasswordTextBox");
        _passwordTextField.setPreferredSize(new Dimension(100,25));
        _passwordTextField.addKeyListener(new TextBoxKeyListener(_label, _passwordTextField));
        _passwordTextField.addMouseListener(new TextBoxMouseAdapter(_label, _passwordTextField));
        _passwordTextField.getDocument().addDocumentListener(new DocumentListener() {
            public void insertUpdate(DocumentEvent e) {
                JTextField textField = (JTextField) _passwordTextField;
                _label.setText(textField.getText());
            }

            public void removeUpdate(DocumentEvent e) {

            }

            public void changedUpdate(DocumentEvent e) {
                JTextField textField = (JTextField) _passwordTextField;
                _label.setText(textField.getText());
            }
        });

        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout(FlowLayout.LEFT));
        panel.setLocation(10,100);
        panel.setSize(400,30);

        panel.add(new Label("Password TextBox"));
        panel.add(_passwordTextField);

        return panel;
    }

    private JPanel createReadOnlyTextBox(){
        JTextField _readOnlyTextField = new JTextField(20);

        _readOnlyTextField.setPreferredSize(new Dimension(190,25));
        //_readOnlyTextField.setLocation(10,40);
        //_readOnlyTextField.setSize(300,30);

        _readOnlyTextField.setVisible(true);
        _readOnlyTextField.setBackground(Color.white);
        _readOnlyTextField.setText("Editable Text Box");
        _readOnlyTextField.setEditable(false);
        _readOnlyTextField.setName("ReadOnlyTextBox");
        _readOnlyTextField.addKeyListener(new TextBoxKeyListener(_label, _readOnlyTextField));
        _readOnlyTextField.addMouseListener(new TextBoxMouseAdapter(_label, _readOnlyTextField));
        _readOnlyTextField.setAlignmentX(Component.CENTER_ALIGNMENT);
        _readOnlyTextField.repaint();


        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout(FlowLayout.LEFT));
        panel.setLocation(10,40);
        panel.setSize(400,30);

        panel.add(new Label("ReadOnly TextBox"));
        panel.add(_readOnlyTextField);

        return panel;
    }

    private  JPanel createTextArea(){
        JTextArea _textArea = new JTextArea(4,20);
        _textArea.setVisible(true);

        _textArea.setPreferredSize(new Dimension(190,25));
        //_textArea.setLocation(10,130);
        //_textArea.setSize(300,75);

        _textArea.repaint();
        _textArea.setBorder(BorderFactory.createLineBorder(Color.black,1));
        _textArea.setText("This is the \n multiline \n text box");
        _textArea.setBackground(Color.white);
        _textArea.setName("MultilineTextBox");
        _textArea.addKeyListener(new TextBoxKeyListener(_label, _textArea));
        _textArea.addMouseListener(new TextBoxMouseAdapter(_label, _textArea));

        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout(FlowLayout.LEFT));
        panel.setLocation(10,130);
        panel.setSize(400,90);

        panel.add(new Label("Multiline TextBox"));
        panel.add(_textArea);

        return panel;
    }

    private  JPanel createDisabledTextBox(){
        JTextField _disabledTextField = new JTextField(20);

        _disabledTextField.setPreferredSize(new Dimension(190,25));
        //_disabledTextField.setLocation(10,70);
        //_disabledTextField.setSize(300,25);

        _disabledTextField.setVisible(true);
        _disabledTextField.setEnabled(false);
        _disabledTextField.setBackground(Color.white);
        _disabledTextField.setText("Disable Text Box");
        _disabledTextField.setName("DisableTExtBox");
        _disabledTextField.addKeyListener(new TextBoxKeyListener(_label, _disabledTextField));
        _disabledTextField.addMouseListener(new TextBoxMouseAdapter(_label, _disabledTextField));

        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout(FlowLayout.LEFT));
        panel.setLocation(10,70);
        panel.setSize(400,30);

        panel.add(new Label("Disabled TextBox"));
        panel.add(_disabledTextField);

        return panel;
    }

    private  JPanel createResizableTextBox(){
        final JTextField _simpleTextField = new JTextField();
        _simpleTextField.setPreferredSize(new Dimension(190,25));

        _simpleTextField.setVisible(true);
        _simpleTextField.setBackground(Color.white);
        _simpleTextField.setText("Resizable TextBox");
        _simpleTextField.setName("Resizable TextBox");
        _simpleTextField.addKeyListener(new TextBoxKeyListener(_label, _simpleTextField));
        _simpleTextField.addMouseListener(new TextBoxMouseAdapter(_label, _simpleTextField));

        //Create Panel
        final JPanel panel = new JPanel();
        panel.setBorder(BorderFactory.createEtchedBorder());
        panel.setLayout(new FlowLayout(FlowLayout.LEFT));
        panel.setLocation(10,220);
        panel.setSize(500,100);

        //Create Location Panel

        final JPanel locationPanel = new JPanel();
        locationPanel.setBorder(BorderFactory.createEtchedBorder());

        final JTextField _xTextField = new JTextField();
        _xTextField.setPreferredSize(new Dimension( 50,25));
        _xTextField.setText(String.valueOf(panel.getLocation().getX()));

        final JTextField _yTextField = new JTextField();
        _yTextField.setPreferredSize(new Dimension( 50,25));
        _yTextField.setText(String.valueOf(panel.getLocation().getY()));

        final JTextField _widthTextField = new JTextField();
        _widthTextField.setPreferredSize(new Dimension( 50,25));
        _widthTextField.setText(Double.toString(_simpleTextField.getPreferredSize().getWidth()));

        final JTextField _heightTextField = new JTextField();
        _heightTextField.setPreferredSize(new Dimension( 50,25));
        _heightTextField.setText(Double.toString(_simpleTextField.getPreferredSize().getHeight()));

        final JButton locationButton = new JButton("Click Me");

        String[] lay = new String[] {"Left", "Right"};
        final  JComboBox layoutCombo = new JComboBox(lay);

        locationButton.addMouseListener(new MouseInputAdapter() {
            public void mouseClicked(MouseEvent e) {

                Double width = new Double(Double.parseDouble(_widthTextField.getText()));
                Double height = new Double(Double.parseDouble(_widthTextField.getText()));

                _simpleTextField.setPreferredSize(new Dimension(width.intValue(), height.intValue()));
                if(layoutCombo.getSelectedItem().equals("Right"))
                    panel.setLayout(new FlowLayout(FlowLayout.RIGHT));
                else
                    panel.setLayout(new FlowLayout(FlowLayout.LEFT));

                Double left = new Double(Double.parseDouble(_xTextField.getText()));
                Double top = new Double(Double.parseDouble(_yTextField.getText()));

                if(left.intValue() != 0 && top.intValue() != 0) {
                    panel.setLocation(left.intValue(), top.intValue());
                }
            }
        });

        locationPanel.add(new JLabel("X:"));
        locationPanel.add(_xTextField);
        locationPanel.add(new JLabel("Y:"));
        locationPanel.add(_yTextField);
        locationPanel.add(new JLabel("Width:"));
        locationPanel.add(_widthTextField);
        locationPanel.add(new JLabel("Height:"));
        locationPanel.add(_heightTextField);
        locationPanel.add(layoutCombo);
        locationPanel.add(locationButton);


        //Add all controls
        panel.add(new Label("Resiable TextBox"));
        panel.add(_simpleTextField);
        panel.add(locationPanel);

        return panel;
    }
}
