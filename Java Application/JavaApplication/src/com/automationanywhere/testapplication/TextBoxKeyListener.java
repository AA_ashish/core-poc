package com.automationanywhere.testapplication;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Created by Jecky.Kansara on 17-02-2017.
 */
public class TextBoxKeyListener implements KeyListener {

    JLabel _label;
    JTextComponent _textbox;

    public TextBoxKeyListener(JLabel label, JTextComponent textBox)
    {
        _label = label;
        _textbox = textBox;
    }

    public void keyTyped(KeyEvent e) {
        _label.setText(_textbox.getText());
    }

    public void keyPressed(KeyEvent e) {
        _label.setText(_textbox.getText());
    }

    public void keyReleased(KeyEvent e) {
        _label.setText(_textbox.getText());
    }
}
