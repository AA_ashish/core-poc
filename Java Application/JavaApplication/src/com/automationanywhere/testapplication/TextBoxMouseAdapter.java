package com.automationanywhere.testapplication;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by Jecky.Kansara on 17-02-2017.
 */
public class TextBoxMouseAdapter extends MouseAdapter {
    JLabel _label;
    JComponent _textComponent;
    public TextBoxMouseAdapter(JLabel label, JComponent textComponent)
    {
        _label = label;
        _textComponent = textComponent;
    }

    public void mouseClicked(MouseEvent e) {
        if(e.getButton() == 1) {
            if(e.getClickCount() == 1) {
                _label.setText("Left click is performed on " + _textComponent.getName());
            }
            else
            {
                _label.setText("Double click is performed on " +  _textComponent.getName());
            }
        }
        else if(e.getButton() == 3)
        {
            _label.setText("Right click is performed on " + _textComponent.getName());
        }
    }

    public void mousePressed(MouseEvent e) {

    }

    public void mouseReleased(MouseEvent e) {

    }

    public void mouseEntered(MouseEvent e) {

    }

    public void mouseExited(MouseEvent e) {
    }

}
