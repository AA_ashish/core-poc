package com.automationanywhere.testapplication;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by Jecky.Kansara on 09-03-2017.
 */
public class ToolBarMouseAdapter extends MouseAdapter {

    JFrame _frame;
    JPanel _currentPanel;
    static MouseEvent event;
    public ToolBarMouseAdapter(JFrame frame)
    {
       _frame = frame;
    }

    public void mouseClicked(MouseEvent e) {
        event = e;
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                if (event.getButton() == 1) {

                    if (_currentPanel != null)
                        _frame.remove(_currentPanel);

                    if (event.getComponent().getName().equals("ToolBar_TextBox")) {
                        _currentPanel = new TextBoxPanel();
                        _frame.add(BorderLayout.CENTER, _currentPanel);
                    } else if (event.getComponent().getName().equals("ToolBar_Tab")) {
                        _currentPanel = new JPanel();
                        _currentPanel.add(new Button("TabPanel"));
                        _frame.add(BorderLayout.CENTER, _currentPanel);
                    }

                    _frame.repaint();
                }
            }
        });
    }




    public void mousePressed(MouseEvent e) {

    }

    public void mouseReleased(MouseEvent e) {

    }

    public void mouseEntered(MouseEvent e) {

    }

    public void mouseExited(MouseEvent e) {
    }
}
